# HSE Advanced C++ course

* [Как начать работу с курсом](docs/setup.md)
* [Решение распространённых проблем](docs/troubleshooting.md)
* [Веб-интерфейс с дедлайнами и оценками](https://cpp-hse.net)
