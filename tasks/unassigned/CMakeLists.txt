# 2
add_subdirectory(list)

# 3
add_subdirectory(dungeon)
add_subdirectory(fold)

# 5
add_subdirectory(grep)
add_subdirectory(safe-transform)
add_subdirectory(tryhard)
add_subdirectory(defer)

# 6
add_subdirectory(pimpl)
add_subdirectory(small-test-framework)
add_subdirectory(editor)
add_subdirectory(any)
add_subdirectory(scala-vector)

# 7
add_subdirectory(constexpr-map)
add_subdirectory(compile-eval)

# 8
add_subdirectory(is-prime)
add_subdirectory(reduce)
add_subdirectory(subset-sum)
add_subdirectory(hash-table)

# 9
add_subdirectory(timerqueue)
add_subdirectory(semaphore)
add_subdirectory(rw-lock)
add_subdirectory(buffered-channel)
add_subdirectory(unbuffered-channel)

# 10
add_subdirectory(futex)
add_subdirectory(mpsc-stack)
add_subdirectory(fast-queue)
add_subdirectory(rw-spinlock)

# 11
# add_subdirectory(coroutine)
# add_subdirectory(generator)

# Bonuses
add_subdirectory(solve-or-die)
add_subdirectory(matrix-2.0)
add_subdirectory(concepts)
add_subdirectory(executors)
