# Unassigned tasks
add_subdirectory(unassigned)

# Weekly homeworks
add_subdirectory(intro)
add_subdirectory(memory)
add_subdirectory(move)

# Big homeworks
add_subdirectory(smart-ptrs)
add_subdirectory(scheme)
# add_subdirectory(jpeg-decoder)

# Bonus
add_subdirectory(bonus)
